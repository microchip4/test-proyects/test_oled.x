/**
 * SSD1306xLED - Drivers for SSD1306 controlled dot matrix OLED/PLED 128x64 displays
 *
 * @created: 2014-08-12
 * @author: Neven Boyanov, Brandon Ruiz Vasquez
 *
 * This is part of the Tinusaur/SSD1306xLED project.
 *
 * Copyright (c) 2016 Neven Boyanov, Tinusaur Team. All Rights Reserved.
 * Distributed as open source software under MIT License, see LICENSE.txt file.
 * Please, as a favor, retain the link http://tinusaur.org to The Tinusaur Project.
 *
 * Source code available at: https://bitbucket.org/tinusaur/ssd1306xled
 *
 */

#ifndef SSD1306XLED_H
#define SSD1306XLED_H

#include <xc.h>
// ============================================================================

// -----(+)--------------->	// Vcc,	Pin 1 on SSD1306 Board
// -----(-)--------------->	// GND,	Pin 2 on SSD1306 Board
//#ifndef SSD1306_SCL
#define SSD1306_SCL         LATBbits.LATB0      // SCL,	Pin 3 on SSD1306 Board
#define SSD1306_SCL_TRIS	TRISBbits.TRISB0	// SCL,	Pin 3 on SSD1306 Board
//#endif
//#ifndef SSD1306_SDA
#define SSD1306_SDA         LATBbits.LATB1      // SDA,	Pin 4 on SSD1306 Board
#define SSD1306_SDA_TRIS	TRISBbits.TRISB1	// SDA,	Pin 4 on SSD1306 Board
//#endif
//#ifndef SSD1306_SA
#define SSD1306_SA		0x78	// Slave address
//#endif

// ----------------------------------------------------------------------------

// These functions are used only internally by the library
void ssd1306_xfer_start(void);
void ssd1306_xfer_stop(void);
void ssd1306_send_byte(char byte);
void ssd1306_send_command(char command);
void ssd1306_send_data_start(void);
void ssd1306_send_data_stop(void);

// ----------------------------------------------------------------------------

#define ssd1306_clear() ssd1306_fill(0);
#define ssd1306_char(c) ssd1306_char_font6x8(c)
#define ssd1306_string(s) ssd1306_string_font6x8(s)
#define ssd1306_numdec(n) ssd1306_numdec_font6x8(n)
#define ssd1306_numdecp(n) ssd1306_numdecp_font6x8(n)

// ----------------------------------------------------------------------------

void ssd1306_init(void);
void ssd1306_setpos(char x, char y);
void ssd1306_fill4(char p1, char p2, char p3, char p4);
void ssd1306_fill2(char p1, char p2);
void ssd1306_fill(char p);
void ssd1306_fillscreen(char fill);
void ssd1306_char_font6x8(char ch);
void ssd1306_string_font6x8(char *s);
void ssd1306_numdec_font6x8(int num);
void ssd1306_numdecp_font6x8(int num);
void ssd1306_draw_bmp(char x0, char y0, char x1, char y1, const char bitmap[]);

// ============================================================================

#endif
